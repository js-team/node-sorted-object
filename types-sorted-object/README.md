# Installation
> `npm install --save @types/sorted-object`

# Summary
This package contains type definitions for sorted-object (https://github.com/domenic/sorted-object#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/sorted-object.

### Additional Details
 * Last updated: Wed, 15 Jan 2020 22:38:02 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Richie Bendall (https://github.com/Richienb).
